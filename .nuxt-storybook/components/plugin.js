import Vue from 'vue'
import { wrapFunctional } from './index'

const components = {
  Logo: () => import('../../components/Logo.vue' /* webpackChunkName: "components/logo" */).then(c => wrapFunctional(c.default || c)),
  Button: () => import('../../components/button.vue' /* webpackChunkName: "components/button" */).then(c => wrapFunctional(c.default || c)),
  Link: () => import('../../components/link.vue' /* webpackChunkName: "components/link" */).then(c => wrapFunctional(c.default || c))
}

for (const name in components) {
  Vue.component(name, components[name])
  Vue.component('Lazy' + name, components[name])
}
