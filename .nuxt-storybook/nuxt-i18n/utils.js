import { localeMessages } from './options'
import { formatMessage } from './utils-common'

/**
 * Asynchronously load messages from translation files
 *
 * @param {import('@nuxt/types').Context} context
 * @param {string} locale Language code to load
 * @return {Promise<void>}
 */
export async function loadLanguageAsync (context, locale) {
  const { app } = context
  const { i18n } = app

  if (!i18n.loadedLanguages) {
    i18n.loadedLanguages = []
  }

  if (!i18n.loadedLanguages.includes(locale)) {
    const localeObject = /** @type {import('../../types').LocaleObject[]} */(i18n.locales).find(l => l.code === locale)
    if (localeObject) {
      const { file } = localeObject
      if (file) {
        /*  */
      } else {
        // eslint-disable-next-line no-console
        console.warn(formatMessage(`Could not find lang file for locale ${locale}`))
      }
    }
  }
}
