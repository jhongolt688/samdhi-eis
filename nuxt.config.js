import en from './locales/en.json'
import de from './locales/de.json'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'samadhi-eis',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: ''
      },
    ],
    link: [{
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css2?family=Handlee&family=Nunito:wght@600&display=swap",
      },

    ],
  },

  storybook: {
    stories: [
      '~/stories/**/*.stories.js',
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],



  styleResources: {
    scss: [

      '~/assets/scss/variables.scss',
      '~/assets/scss/reset.scss',
    ]
  },



  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    // '@nuxtjs/eslint-module',
  ],



  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    'nuxt-i18n',
    '@nuxtjs/style-resources',
  ],
  i18n: {
    locales: ['de', 'en'],
    defaultLocale:"en",
    vueI18n: {
      fallbackLocale: 'de',
      messages: {
        en,
        de
      }
    }
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

};
